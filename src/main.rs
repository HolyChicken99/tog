#[allow(non_camel_case_types)]
// The current instruction set allows Add, Push ,POP, DUMP
enum OPS {
    ADD,
    PSH,
    POP,
    DUMP,
}
struct INST {
    Num: i32,
    Operation: OPS,
}

fn forward_pass(vec: Vec<INST>) {
    // arr represents the value stack
    let mut arr: [i32; 100] = [0; 100];
    let mut length: i32 = 0;
    for instruction in vec.iter() {
        match &instruction.Operation {
            ADD => {
                if length < 2 {
                    println!("not sufficient elements");
                    break;
                } else {
                    arr[0] += arr[1];
                    length -= 1;
                }
            }

            PSH => {
                arr[length as usize] = instruction.Num;
                length += 1;
            },
            POP => {

///TODO implement forward copying


            },
            _ => println!("Aint special"),
        }
    }
}

fn main() {
    let mut v: Vec<INST> = Vec::new();
    v.push(INST {
        Num: 45,
        Operation: OPS::PSH,
    });
    forward_pass(v);
}
